# A Dred End

This 7 Days to Die Mod is a work in progress towards a more logical, scientific, deeper, scary and adapting version of the base game.

The goal of this mod is to expand on the base game to allow the player adapt to a changing, dynamic world. 
To explore and exploit will be part of this mod.
There will be things you need to go out and find so that you can adapt to the world you now live in. 
If you do not adapt this world will kill you.
Through the use of research you will unlock new techniques, items, blocks, powers and knowledge.

NOTE: This is a work in progress. I have a very long list of thing to implement and I am adding them as I have time and knowledge of how to make them. 
At the moment the game is in an early testing state to see how the systems I have built work so far.
I will be altering this to be an SDX mod eventually. 
There will also be changes to the progression system, skills and quests all which could break save games.
The goal is to have a mod that is not as hard as true survial early game but will certainly get more difficult.
The zombies will eventually be a ever present background threat, but new entities from other m-branes will attempt to make the world theirs.
I will endeavour to only release world or character breaking updates as semi major updates. 
This way people will know if a build will break their game and will not break their games.

**An Ending:**\
I have added one quest that should be an end condition of the game. Go out and destroy the infected cloning facility. 
It wont stop all the zombies of the world but it will greatly reduce the number in your county. (When I am done)\
Still working on polishing this feature, I also need a little bit of SDX magic to finish this.

**Research system:**\
Create a research desk and start crafting experiments. 
Some of the items that you find in the world are new and will allow you unlock new ideas and schematics that can lead to new abilities, blocks and items. 

**Reduced gameyness:**\
There are some things in the game that feel like are only in there to make certain reward cycles work.\
I aim to reduce this to give the game a greater feeling of immersion.
- The addition of Respirocytes to give a bonus to early game stamina, and provide the player extra stamina in the end game.
- Bicycles in garages. Like cars, bikes live there too.

**New Weapons:**\
Many Planned, The mod was going to be a melee mod and thus has new weapons and there are many planned in updates.

- Thagomizer a new club based on the tail of a stegosaurus. Should be a one shot on easy zombies with a hard swing.
- Spears, There is a stick that can be sharpened and there is also an Iron spear that is craftable to give even more range and damage.
 
**Handles and mods**\
I have added a few simple handles that with some luck you will be able to upgrade into tools 
It is planned to have it so that the handle will have more weapon options as well.

**Melee signs:**\
It should be the case that all sings in the game can be weapons. All you need to do is break them free from there bonds and start smashing :)

**Zombie Changes:**\
I giveth and taketh
- Better Eyesight - The zombies have a range of 100 so they will be coming for you. Not bad during the day, but at night they run and will swarm you.
- Reduced melee range - The zombies Arm reach is now from the center of their boides to the end of one of their hands
- Headshots - They have soft heads. Some weapons will smoosh it. 
- body shots - Why do they need organs, so you can smack them, put holes in them break them to a point. The torso is tough 
- Increased spawning numbers - There will be more zombies. 
- Random getter up - The zombies will take 1 second to 10 seconds to get back up if they have be knocked down.
- No hurt sound, no death sound - in combination with the random getter upper can lead to some surprises so don't forget, Double tap.

**Noise makers:**\
These are little distractors that can be thrown to attract zombies.

**Lights:**\
Most of the lights in the game have been added to craft

**New top tier Block**
Zombzyme block is a very resilient block created from various household chemicals and the arm bones of zombies. 
It will take some research and experimentation to figure out how to do this

**SPIKES from alpha 16.4**
I have brought back the ability to craft the spikes from alpha 16.
I have also expanded it with the ability to craft quicker crappier versions of the spikes too. Just incase you are in a pinch for time.

**Progression**
I do want to make major changes to this but atm it is mostly vanilla a17. 
There is though the option of finding some of the schematics for various items throughout the world. 


Thanks for reading this. I hope you enjoy it. Let me know any bugs on my discord. 
I currently have a list of things from thumbnails and localizations to unity fixes and Code alterations to get to. 



In terms of licence and the reuse. 
Some of the models are under different licence I will attempt to sort all of it out. For reuse contact me @ gmail, or on my discord.
Personal reuse is likely all ok :)


Other Modders I would like to thank are:
GuppyCurr, Khaine, Keledon, Haidr Gna, Spherrii, Stopmy NZ, Xyth, Darkstar Dragon, DanCap0, DeathtoDust, Mumphy, Manux. 
Really most of the modders on the guppy server :)




I am going to me working on the formatting of the page too
https://guides.github.com/features/mastering-markdown/

